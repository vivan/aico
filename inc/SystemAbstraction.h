/*
 * SystemAbstraction.h
 *
 *  Created on: 14 Aug 2013
 *      Author: s0972326
 */

#ifndef SYSTEMABSTRACTION_H_
#define SYSTEMABSTRACTION_H_

namespace SOC {

class SystemAbstraction {
public:
	SystemAbstraction();
	virtual ~SystemAbstraction();
};

} /* namespace SOC */
#endif /* SYSTEMABSTRACTION_H_ */
